package server;

import javax.net.ssl.*;
import java.io.*;


public class Connection {
    /**
     * This class handles the communication between 2 clients.
     * */
    private SSLSocket connection_1;
    private SSLSocket connection_2;

    private DataInputStream client1_input;
    private DataInputStream client2_input;
    private DataOutputStream client1_output;
    private DataOutputStream client2_output;

    public void init()
    {
    // Invoking a thread for each player. Both threads get an SSLSocket object
    // and a hardcoded boolean. The booleans are used to identify these two threads
    // and determine the physics-engine host. (`false` => player_1, `true` => player_2)
        try {
        // Waits for both sockets to be connected.
            connection_1 = (SSLSocket) Server.server.accept();
            System.out.println("Client_1 attempts to connect");

            connection_2 = (SSLSocket) Server.server.accept();
            System.out.println("Client_2 attempts to connect");

        // Initializing input/output objects for client threads.
            client1_input = new DataInputStream(connection_1.getInputStream());
            client2_input = new DataInputStream(connection_2.getInputStream());
            client1_output = new DataOutputStream(connection_1.getOutputStream());
            client2_output = new DataOutputStream(connection_2.getOutputStream());

            connection_1.setUseClientMode(false);
            connection_2.setUseClientMode(false);

            ClientThread_1 client_1 = new ClientThread_1();
            ClientThread_2 client_2 = new ClientThread_2();

            if (!connection_1.isClosed() && !connection_2.isClosed())
            {
                client_1.start();
                client_2.start();
            }
        // *Both client threads are hardcoded. This way only 2 can connect to the game.
        }
        catch (Exception e) {
            System.out.println("Clients couldn't connect to each other.");
        // Closes the connection for both sockets.
            try {connection_1.close();}catch(Exception e1){}
            try {connection_2.close();}catch(Exception e2){}
            e.printStackTrace();
        }
    }

    private class ClientThread_1 extends Thread {
        private final boolean thread_id;

        ClientThread_1() {
            this.thread_id = false;
        }

        public void run()
        {
            try {
                connection_1.setEnabledProtocols(new String[] {"TLSv1.3", "TLSv1.2"});
                connection_1.startHandshake(); // Waiting for the handshake before allowing input.

            // This is an extra layer of security that receives a value and makes
            // sure it was sent by the client software. It's most likely unnecessary.
                long client_key;

                while (true) {
                    client_key = client1_input.readLong();
                    if (client_key % 17 == 0) {
                        break;
                    } else {
                        client1_input.close();
                        client1_output.close();
                        break;
                    }
                }
                client1_output.flush();

            // The thread is now ready to receive data from client.
                System.out.println(connection_1.getInetAddress()+" connected.");

            // First we decide which client will host the physics engine.
                client1_output.writeBoolean(thread_id);
                client1_output.flush();

            // Pass the `unique_number` as a test.
                client2_output.writeInt(client1_input.readInt());
                client2_output.flush();

            // Main loop for threaded connection.
                while(true) {
                    client2_output.writeShort(client1_input.readShort());
                    client2_output.writeShort(client1_input.readShort());
                    client2_output.writeShort(client1_input.readShort());
                    client2_output.writeShort(client1_input.readShort());
                }
            }
            catch (SSLHandshakeException shse) {
                System.out.println(connection_1.getInetAddress()+" tried and failed to connect (handshake fault)");
                shse.printStackTrace();
            }
            catch (IOException ioe) {
                System.out.println(connection_1.getInetAddress()+" disconnected.");
                ioe.printStackTrace();
            }
            catch (Exception e) {
                System.out.println(connection_1.getInetAddress()+" connection failed.");
                e.printStackTrace();
            }
            finally {
                try {connection_1.close();}catch(Exception e){}
                try {connection_2.close();}catch(Exception e){}
            }
        }
    }

    private class ClientThread_2 extends Thread {
        private final boolean thread_id;

        ClientThread_2() {
            this.thread_id = false;
        }

        public void run()
        {
            try {
                connection_2.setEnabledProtocols(new String[] {"TLSv1.3", "TLSv1.2"});
                connection_2.startHandshake(); // Waiting for the handshake before allowing input.

            // This is an extra layer of security that receives a value and makes
            // sure it was sent by the client software. It's most likely unnecessary.
                long client_key;

                while (true) {
                    client_key = client2_input.readLong();
                    if (client_key % 17 == 0) {
                        break;
                    } else {
                        client2_input.close();
                        client2_output.close();
                        break;
                    }
                }
                client2_output.flush();

            // The thread is now ready to receive data from client.
                System.out.println(connection_2.getInetAddress()+" connected.");

            // First we decide which client will host the physics engine.
                client2_output.writeBoolean(this.thread_id);
                client2_output.flush();

            // Pass the `unique_number` as a test.
                client1_output.writeInt(client2_input.readInt());
                client1_output.flush();

            // Main loop for threaded connection.
                while(true) {
                    client1_output.writeShort(client2_input.readShort());
                    client1_output.writeShort(client2_input.readShort());
                    client1_output.writeShort(client2_input.readShort());
                    client1_output.writeShort(client2_input.readShort());
                }
            }
            catch (SSLHandshakeException shse) {
                System.out.println(connection_2.getInetAddress()+" tried and failed to connect (handshake fault)");
                shse.printStackTrace();
            }
            catch (IOException ioe) {
                System.out.println(connection_2.getInetAddress()+" disconnected.");
                ioe.printStackTrace();
            }
            catch (Exception e) {
                System.out.println(connection_1.getInetAddress()+" connection failed.");
                e.printStackTrace();
            }
            finally {
                try {connection_2.close();}catch(Exception e){}
                try {connection_1.close();}catch(Exception e){}
            }
        }
    }
}
