package test_client;

import java.io.*;
import java.net.*;
import javax.net.ssl.*;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;


public class Client { //"85.23.144.26" <- add later.
    public static void main(String...args) throws IOException, InterruptedException
    {
    // Clearing the command prompt/terminal before running client.
        new ProcessBuilder("clear").inheritIO().start().waitFor();

        SSLSocketFactory factory = (SSLSocketFactory) SSLSocketFactory.getDefault();
        SSLSocket connection = (SSLSocket) factory.createSocket("127.0.0.1", 3010);
        System.out.println("Connecting.."); // At this point the socket is connected.

    // Enabling ciphers. Non-used are commented out.
        connection.setEnabledCipherSuites(new String[] {
          //"TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256", //! ChaCha is not yet
          //"TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256",   // available for TLS !
            "TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256",
            "TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384",
            "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
            "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384",
            "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA",
            "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA",
            "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA",
            "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA",
          //"TLS_RSA_WITH_AES_128_GCM_SHA256",
          //"TLS_RSA_WITH_AES_256_GCM_SHA384",
          //"TLS_RSA_WITH_AES_128_CBC_SHA",
          //"TLS_RSA_WITH_AES_256_CBC_SHA"
        });

        connection.setEnabledProtocols(new String[]{
                "TLSv1.3",
                "TLSv1.2"
        });
        connection.setUseClientMode(true); // <- Not sure what it does.

        DataOutputStream output = new DataOutputStream(connection.getOutputStream());
        DataInputStream input = new DataInputStream(connection.getInputStream());

        connection.startHandshake(); // Waiting for handshake to be carried successfully.

    // This is just an extra layer of security to check
    // from server-side whether client program is authentic.
        long seeded_random = ThreadLocalRandom.current().nextLong(542551296285575047L);
        boolean bool_random = ThreadLocalRandom.current().nextBoolean();

        seeded_random *= bool_random ? 17 : -17;
        output.writeLong(seeded_random);
        output.flush();

    // At this point the client waits for server check
    // the value with % 17. After that we are good to go.

        try {
            Runnable input_reader = new Runnable() {
                public void run() {
                    try {
                        while (true) {
                            System.out.println(input.readShort());
                        }
                    } catch(IOException ioe) {
                        System.out.println("Connection closed");
                    }
                }
            };

        // Starting a thread to handle reading from input stream.
            new Thread(input_reader).start();

            while (true) { // This is the main loop for writing and sending values.
                Scanner scanner = new Scanner(System.in);
                output.writeShort(scanner.nextShort());
            }

        // The connection closes and the program stops only after some exception occurs.

        } catch(InputMismatchException ime)
        {
        // If the value is greater than 32767 or less than -32768, connection ends.
            input.close(); output.close(); connection.close();
            System.out.println("Warning: Short can't hold this value!");
            throw ime;

        } catch(SocketException se)
        {
        // Basic socket exception. Usually comes when there is an interruption.
            input.close(); output.close(); connection.close();
            System.out.println("Connection closed.");

        // This hack makes sure that the socket is closed after exception occurs.
            try {connection.close();}catch(IOException ioe){}
            throw se;
        }
    }
}
