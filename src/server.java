import java.io.IOException;
import java.lang.ArrayIndexOutOfBoundsException;

import server.Server;


public class server {
    /**
     * Use `java server start` to run server.class
     * from the output. This will start the server.
     * */
    public static void main(String...args) throws IOException
    {
        final String version = "0.2.0";

        final String info = "\nUsage: server <argument>\n" +
                "\nCommands: start, help, version\n" +
                "\n server start      - start the server" +
                "\n server --help     - display help message" +
                "\n server --version  - show current version number\n";

        try {
            switch (args[0]) {

                case "start":
                    System.out.print("\nStaring server..\n");
                    Server.start();
                    break;

                case "--help":
                    System.out.print(info);
                    break;

                case "--version":
                    System.out.print("Server version: "+version+"\n");
                    break;

                default:
                    System.out.print("Invalid command '"+args[0]+"'\n"+info);
            }
        } catch(ArrayIndexOutOfBoundsException aie) {
            System.out.print("No arguments were given.\n"+info);
        }
    }
}
