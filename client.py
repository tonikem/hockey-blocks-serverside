import subprocess as sub


sub.run("cd ./out/production/hockey-blocks-serverside && java "
        "-Djavax.net.ssl.keyStore=./test_client/keystore/clientKeyStore \\"
        "-Djavax.net.ssl.keyStorePassword=+ranged9 \\"
        "-Djavax.net.ssl.trustStore=./test_client/keystore/clientTrustStore \\"
        "-Djavax.net.ssl.trustStorePassword=+ranged9 \\"
        "test_client.Client -ssl peerAuthentication", shell=True)
