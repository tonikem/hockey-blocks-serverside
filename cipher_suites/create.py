server_suites = ""
supported_suites = ""

file = open("./server_suites.txt", "r")
server_suites += file.read()
file.close()

file = open("./android_suites.txt", "r")
android_suites = file.read().split("\n")
file.close()

suites = server_suites.split("\n")

for suite in suites:
    if suite in android_suites:
        supported_suites += suite.replace("Cipher Suite: ", "") + "\n"
    else:
        continue

file = open("./supported.txt", "w")
file.write(supported_suites)
file.close()
