import subprocess as sub


sub.run("cd /home/server/HB-server/hockey-blocks-serverside/out/production/hockey-blocks-serverside && java "
        "-Djavax.net.ssl.keyStore=./server/keystore/serverKeyStore \\"
        "-Djavax.net.ssl.keyStorePassword=+ranged9 \\"
        "-Djavax.net.ssl.trustStore=./server/keystore/serverTrustStore \\"
        "-Djavax.net.ssl.trustStorePassword=+ranged9 \\"
        "server start -ssl peerAuthentication", shell=True)
